package com.alkes.factories;

import org.adempiere.base.IProcessFactory;
import org.compiere.process.ProcessCall;
import org.compiere.util.CLogger;


public class AlkesCustomProcessFactory implements IProcessFactory{
    public final static CLogger log = CLogger.getCLogger(AlkesCustomProcessFactory.class);
    
    public ProcessCall newProcessInstance(String className) {
        if (className.equals("com.alkes.report.BankRegisterCustom")) {
            return new com.alkes.report.BankRegisterCustom();
        }
        return null;
    }
}

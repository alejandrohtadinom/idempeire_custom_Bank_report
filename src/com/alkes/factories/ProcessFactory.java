package com.alkes.factories;

import org.adempiere.base.AnnotationBasedProcessFactory;

public class ProcessFactory extends AnnotationBasedProcessFactory{
    
        @Override
        protected String[] getPackages() {
            return new String[] {"com.alkes.process"};
        }
    
}
